## Step 1 - Build và khơi chạy
- Mở **cmd** cùng cấp với file **docker-compose.yml**
```bash
#1
docker-compose build
#2
docker-compose up -d

#If using linux:
sudo chown -R $USER nginx/logs

## Step 2 - Truy cập vào terminal của containter main
```bash
#1 Truy cập vào container main
docker exec -it laravel bash

#2 Cập nhật packages
composer update

#If using linux:
#Cấp quyền cho các thư mục
sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache
sudo chmod -R 775 storage
sudo chmod -R 775 bootstrap/cache
```

## Step 3 - Config:
#Tạo env theo mẫu
cp .env.example .env

#Chỉnh sửa .env
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=shopdb
DB_USERNAME=root
DB_PASSWORD=password
```

Truy cập `http://localhost:3005`

## *Using: 
- Thao tác các lệnh `php artisan` hay `composer install`, `npm install`:
```bash
docker exec -it laravel bash
```

- Thao tác lệnh với **database**:
```bash
docker exec -it mysql bash
```

- Truy cập trình quản lý database mặc định **phpadmin**: `http://localhost:7755`




